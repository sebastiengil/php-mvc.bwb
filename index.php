<?php


spl_autoload_register(function ($class) {
 if(file_exists('./core/' . $class . '.php')):
 include './core/' . $class . '.php';
 elseif (file_exists('./models/' . $class . '.php')):
 include './models/' . $class . '.php';
 elseif (file_exists('./controllers/' . $class . '.php')):
 include './controllers/' . $class . '.php';
 elseif (file_exists('./DAO/' . $class . '.php')):
 include './DAO/' . $class . '.php';
 endif;
});

// spl_autoload_register('chargerClasse');
// spl_autoload_register('chargerClasse2');
// spl_autoload_register('chargerClasse3');

// $dao = new DAO1();
// $dao->getall();

// print_r($dao);