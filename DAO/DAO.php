<?php

abstract class DAO implements CRUDInterface, RepositoryInterface{

   
   
   protected $_pdo;
   
   public function __construct(){
    //    $this->_pdo = new PDO('mysql:dbname=car; host=localhost', 'phpmyadmin', 'root');
      
    //    $config = json_decode(file_get_contents('./config/database.json'), true);
       
    //    var_dump($config);
       
    //    $this->_pdo = new PDO($config['driver'] . ':dbname=' . $config['dbname'] . ';host=' . $config['host'], $config['username'], $config['password']);
       
    //    return $config;
   }
   abstract public function retrieve($id);
   
   abstract public function update($id);
   
   abstract public function delete($id);
   
   abstract public function create($array);
   
   abstract public function getall();
   
   abstract public function getallby();
}